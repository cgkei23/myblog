####以下记录为在mac环境进行的操作
#### 安装micro
```
##该处配置方便拉取go包时，因墙的原因无法获取三方包，配置代理，参考：http://goproxy.io
export GO111MODULE=on
export GOPROXY=https://goproxy.io
source ~/.bash_profile 
# 使用如下指令安装micro。-u 表示只更新需要更新的
go get -u -v github.com/micro/micro
go get -u -v github.com/micro/go-micro
```
#### 安装protoc
访问如下网址,选择和自己环境一致的安装包，这里选择面向objective：protobuf-objectivec-3.11.2.zip
[github链接地址](https://github.com/protocolbuffers/protobuf/releases)
* 需要安装automake
```
brew install automake -- verbose
```
- cd到下载的目录,依次执行
```
$ ./autogen.sh    获取GoogleMock，并生成对应的configure脚本

$ ./configure       进行环境检测，并生成对应的makefile或Makefile(--prefix=/usr/local可以指定安装路径,可以不指定使用默认的)

$ make                按照makefile编译工程

$ make check

$ sudo make install    执行makefile里面的install部分，进行安装(--prefix=/usr/local可以指定安装路径,可以不指定使用默认的)

终端中执行：protoc --version

如果出现libprotoc 3.5.0则说明安装成功
```
[Protocol buffer 3教程参考](https://blog.csdn.net/hulinku/article/details/80827018)
#### 安装protoc-gen-micro插件
安装protobuf依赖

这个插件主要作用是通过.proto文件生成适用于go-micro的代码
```
go get -u -v github.com/micro/protoc-gen-micro
```

#### 安装服务发现consul
通过docker镜像安装
```
docker pull consul
```
##### 启动server端
```
docker run -d -p 8500:8500 -e CONSUL_BIND_INTERFACE='eth0' --name=consul_server_1 consul agent -server -bootstrap -ui -node=testNode -client='0.0.0.0'
```
#### 启动client端
```
docker run -d -e CONSUL_BIND_INTERFACE='eth0' --name=consul_server_4 consul agent -client -join='172.17.0.2' -client='0.0.0.0'
```
##### Consul 命令简单介绍
- agent : 表示启动 Agent 进程。
-server：表示启动 Consul Server 模式。
- -client：表示启动 Consul Cilent 模式。
- -bootstrap：表示这个节点是 Server-Leader ，每个数据中心只能运行一台服务器。技术角度上讲 Leader 是通过 Raft 算法选举的，但是集群第一次启动时需要一个引导 Leader，在引导群集后，建议不要使用此标志。
- -ui：表示启动 Web UI 管理器，默认开放端口 8500，所以上面使用 Docker 命令把 8500 端口对外开放。
- -node：节点的名称，集群中必须是唯一的。
- -client：表示 Consul 将绑定客户端接口的地址，0.0.0.0 表示所有地址都可以访问。
- -join：表示加入到某一个集群中去。 如：-json=192.168.1.23
- [参考资料](https://www.cnblogs.com/lfzm/p/10633595.html)

#### 创建helloword的微服务
```
micro new hello
```
* 编译proto
```
protoc --proto_path=$GOPATH/src:. --micro_out=. --go_out=. ./proto/hello/hello.proto 
```
在项目根目录下运行，生成两个模版文件：

一个是proto的go结构文件：*.pb.go

一个是go-micro的rpc的接口文件：*.micro.go

* 微服务
