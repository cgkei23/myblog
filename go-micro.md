# platform-api

#### 介绍

基于[go-micro](https://github.com/micro/go-micro)微服务框架开发的中台API。

#### 架构图

![avatar](https://gitlab.com/cgkei23/myblog/raw/master/source/operat-arch.png)
![avatar](https://gitlab.com/cgkei23/myblog/raw/master/source/logical-arch.png)

![avatar](https://gitlab.com/cgkei23/myblog/raw/master/source/stat-arch.png)

![avatar](https://gitlab.com/cgkei23/myblog/raw/master/source/cpt-flow.png)

#### 环境

1. go 1.13, ``brew install go@1.13``

#### 运行

1. 本地安装go、docker、docker-compose运行环境
2. 在项目根目录运行 `go get .` 安装所有依赖
3. 在项目跟目录运行 `make run` 启动项目
4. 访问[http://localhost:8082](http://localhost:8082)打开web控制台
5. 使用postman调用[http://localhost:8080](http://localhost:8080)请求网关接口

#### 服务治理

1. 本地环境
   1. [注册和配置中心](http://localhost:8500)
   2. [调用链监控中心](http://localhost:16686)
   3. [服务查看](http://localhost:8082/)
   4. [clickhouse客户端](http://localhost:88)
2. 开发环境（线上）
   1. [注册和配置中心](http://srv-consul-dev.spacesforce.com)
   2. [服务查看](http://srv-web-dev.spacesforce.com/)

#### 服务

1. [服务网关](https://gitlab.spacesforce.com/platform/api-gateway)
2. [用户服务](https://gitlab.spacesforce.com/platform/user-srv)
3. [用户API](https://gitlab.spacesforce.com/platform/user-api)
4. [认证服务](https://gitlab.spacesforce.com/platform/auth-srv)
5. [权限服务](https://gitlab.spacesforce.com/platform/perm-srv)
6. [文件服务](https://gitlab.spacesforce.com/platform/file-srv)
7. [文件API](https://gitlab.spacesforce.com/platform/file-api)

#### 第三方

1. [docker-compose](https://docs.docker.com/compose/)，单机编排
2. [protobuf](https://developers.google.com/protocol-buffers/)，序列化
3. [go-micro](https://github.com/micro/go-micro)，微服务框架
4. [micro](https://github.com/micro/micro)，微服务运行时
5. [OpenTracing](https://wu-sheng.gitbooks.io/opentracing-io/content/)，调用链监控API
6. [jaeger](https://www.jaegertracing.io/docs/1.14/)，调用链监控工具
7. [gRPC](https://doc.oschina.net/grpc?t=56831)，RPC框架
8. [casbin](https://casbin.org/docs/zh-CN/service)，权限控制
9. [govaluate](https://github.com/Knetic/govaluate)，规则引擎
10. [jwt](https://jwt.io/)，身份认证
11. [stow](https://github.com/graymeta/stow)，云存储

#### 开发日志

- 9.10
- [x] 通过micro new分别创建srv、web、api类型的微服务
- [x] 通过make创建微服务网关(api)和控制台(web) 
- [x] 通过postman测试基于HTTP POST调用gateway开放的API接口
- [x] 通过Chrome测试基于web proxy访问开放的web页面
- 9.11
- [x] 通过make编译微服务为二进制文件
- [x] 编写docker-compose的单机编排脚本，包含服务注册中心(consul)、api网关、web控制台、user和doc服务
- 9.16~9.20
- [x] 绘制架构图
- [x] 分离网关到独立仓库
- [x] 实现consul作为**配置中心**，基于go-config实现动态更新配置
- [x] 新增micro网关插件实现**JWT认证**功能
  - [x] 新增User微服务，api类型微服务作为控制层，srv类型微服务为模型层通过gorm访问**DB**
  - [x] 实现模型层接口：用户查询
  - [x] 实现控制层接口：用户登录，用户查询。
- 9.23~9.27 
- [x] 新增micro网关链路追踪插件实现**调用链**的追踪
  - [x] docker-compose新部署jaeger
  - [x] 在User微服务中引入OpenTracing对调用链进行测试
- 10.9~10.18 
- [ ] 新增权限服务

    - [x] 评估支持多租户(域)的RBAC权限模型实现方案，确定基于开源项目casbin进行微服务集成
    - [x] 将casbin包装成micro的服务
    - [x] 将默认的文件存储替换为使用mysql数据库存储casbin权限规则
    - [ ] 实现权限插件对micro网关的请求进行权限验证
- [ ] 新增文件资源服务
    - [x] 评估支持本地和多个云存储品牌的文件管理实现方案，确定基于stow进行微服务集成
    - [ ] 将stow包装为micro的服务 
- 10.21~11.1
- [x] 新增数据统计运行架构图 
- [x] 新增数据采集服务
    - [x] 框架搭建
    - [x] docker compose增加clickhouse server和tabix(clickhouse 可视化 client)
    - [x] 增加ETL的源库和目标库的连接类
    - [x] 引用ratchet包实现基本的数据采集任务
- [x] 新增数据计算服务
    - [x] 框架搭建
    - [x] 增加dataframe-go加载数据源至内存
    - [x] 增加govaluate规则引擎实现通过配置公式计算出指标
    - [x] 增加GoStats扩展govaluate的计算函数
- [ ] 新增任务调度服务
- [ ] 新增数据统计API服务


#### 待深入

1. 怎么支持bpmn？
   1. [bpmn.io](https://github.com/bpmn-io/bpmn-js)，web在线建模
   2. [zeebe](https://docs.zeebe.io/go-client/get-started.html#create-a-workflow-instance)，微服务编排引擎
2. 怎么定义应用组装标准?

#### 开发路线(非功能性需求)

1. 接口文档
2. 单元测试
3. 热加载
4. 认证授权
5. 日志中心
6. 配置中心
7. 监控中心
8. 错误处理
9. 分布式事务处理


