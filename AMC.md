#AMC架构设计及部署流程

|版本|作者|修改时间|备注|
|:---:|:---:|:---:|:---:|
| v1.0 | 李想 | 2019-12-03 |新增|
## 一. AMC技术架构
### AMC网络拓扑图
![avatar](https://gitlab.com/cgkei23/myblog/-/raw/master/source/amc%E7%BD%91%E7%BB%9C%E6%8B%93%E6%89%91.jpg)

#### amc业务完整发布，包含以下几个模块:
1. AMC后端业务项目：amc-back-core
2. AMC后端登录项目：amc-back-login
3. AMC前端业务项目：amc-front-core
4. AMC前端登录项目：amc-front-login
5. 租户后端业务项目：twt-back-core
6. 租户后端登录项目：twt-back-login
7. 租户前端业务项目：twt-front-core
8. 租户前端登录项目：twt-fron-login
9. 微楼书H5项目：amc-h5-brochure
10. 经纪人H5项目： amc-h5-broker

## 二. AMC部署流程

### 2.1 运行环境
|项目|说明|
|:---|:---|
| 操作系统 | Linux |
| web服务器 | nginx/1.16.1 |
| 数据库 | mysql/8.0.18 |
| 开发语言 | php/7.2.24 |

### 2.2 php依赖
```
##业务依赖扩展
Zend OPcache    //将代码缓存用于提升线上访问性能
hash            //使用hash算法进行加密，eg:用户密码加密
curl            //各类curl请求，eg:访问微信服务器校验js_code
redis           //数据缓存
session         //保留会话信息
sockets         //socket消息推送，eg:登录成功提醒
mcrypt          //加密（已弃用，现改为openssl加密）
iconv           //字符串按要求的字符编码来转换
gd              //提供了图像处理功能的各类函数
imagick         //图片处理，eg：合同打印预览
mysqli          //mysqli扩展允许我们访问MySQL 4.1及以上版本提供的功能
mysqlnd         //Mysqlnd是由PHP源码提供的mysql驱动连接代码。它的目的是代替旧的libmysql驱动。
##larvel依赖扩展
openssl         
PDO
pdo_mysql
mbstring
tokenizer
xml
ctype
json

```

### 2.3 Nginx配置
#### 2.3.1 负载分发配置
```
##分发前端登录项目
upstream loginfront {
    server 127.0.0.1:8081;
}
##分发后端登录项目
upstream loginback {
    server 127.0.0.1:8082;
}
##分发前端核心项目
upstream hgvyylst88front {
    server 127.0.0.1:8083;
}
##分发后端登录项目
upstream hgvyylst88back {
    server 127.0.0.1:8084;
}

server{
    listen 80;
    server_name  amc.local.com;

    error_log /usr/local/var/log/nginx/amc.error.log;

    location = / {
        proxy_pass http://amc.local.com/login;
    }
    ##前端登录页面
    location /login {
        proxy_pass http://loginfront/;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header Host $host; # $server_port;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_next_upstream http_500 http_502 http_503 error timeout invalid_header;
    }
    ##后端登录api
    location /login/api {
        proxy_pass http://loginback/;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header Host $host; # $server_port;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_next_upstream http_500 http_502 http_503 error timeout invalid_header;
    }
    ##前端核心页面
    location /hgvyylst88 {
        proxy_pass http://hgvyylst88front/;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header Host $host; # $server_port;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_next_upstream http_500 http_502 http_503 error timeout invalid_header;
    }

    # amc back api
    location /hgvyylst88/api {
                proxy_pass http://hgvyylst88back/;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header X-Forwarded-Port $server_port;
                proxy_set_header Host $host; # $server_port;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_next_upstream http_500 http_502 http_503 error timeout invalid_header;
         }

}
```
#### 2.3.2 各项目模块解析nginx配置
```
## AMC后端核心业务项目：
server {
    listen 8084;

    root /usr/local/var/www/amc.com/amc-back-core/public;

    index index.php;

    server_name _;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
## AMC后端登录项目：
server{
    listen 8082;
    server_name  _;
    root /usr/local/var/www/amc.com/amc-back-login/public;
    index index.php index.html index.htm;
    error_log /usr/local/var/log/nginx/amc-back-log.error.log;

    location / {
       try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
## AMC前端核心业务项目：
server{
    listen 8083;
    server_name  _;
    root /usr/local/var/www/amc.com/amc-front-core/dist;
    index index.html;

    location / {
        try_files $uri $uri/ /index.html;
    }

    location ~ /\.ht {
        deny  all;
    }

    location ~.*\.(js|css|html|png|jpg|svg|json|ttf)$
    {
        expires    3d;
    }
}
## AMC前端登录项目：
server{
    listen 8081;
    server_name  _;
    root /usr/local/var/www/amc.com/amc-front-login/dist;
    index index.html;

    location / {
        try_files $uri $uri/ /index.html;
    }

    location ~ /\.ht {
        deny  all;
    }

    location ~.*\.(js|css|html|png|jpg|svg|json|ttf)$
    {
        expires    3d;
    }
}
```
* 后端每个项目为独立的lavaerl框架的项目
* 每个前端项目为独立的angular/vue项目

### 2.2 各项目环境变量配置
* 后端项目需在项目根目录新建.env文件，进行环境变量配置。

#### 2.2.1 amc-back-core
```
#项目名称
APP_NAME=amc-back-core
#项目ukey
APP_KEY=base64:oTwT/FYU5fIEeAj/kUe9VF09+z0aa3U7Cru6DBLuIRY=
#debug是否开启
APP_DEBUG=true
#日志渠道
LOG_CHANNEL=stack
#应用环境
APP_ENV=dev
#数据库连接配置 (amc-dev 环境)
DB_CONNECTION=mysql
DB_HOST=47.108.52.57
DB_PORT=5712
DB_DATABASE=amc_dev
DB_USERNAME=amc_dev
DB_PASSWORD="4RRQsfS#eXfn#hda"
#缓存驱动方式
CACHE_DRIVER=redis
#session驱动方式
SESSION_DRIVER=file
#队列驱动方式
QUEUE_DRIVER=sync
#memcache配置
MEMCACHE_HOST=127.0.0.1
MEMCACHE_PORT=11211
#redis配置
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
#时区配置
DB_TIMEZONE=+08:00
APP_TIMEZONE=PRC
#项目当前url
CURRENT_URL=https://amc-dev.spacesforce.com/api/
#业务超管访问url
SFO_SERVICE_URL=https://sam-dev.spacesforce.com/api
#七牛云配置
QN_ACCESS_KEY=KOwgBwHvXPRToWUTNPlUla28Nbe5k8OwVKEIb9lG
QN_SECRET_KEY=ELl_VfTSyqrcXQPAjkgMNq0iN39MZtQodmJmhDGn
QN_OUTER_CHAIN_PUBLIC=https://sfo-public.spacesforce.com
QN_OUTER_CHAIN_PRIVATE=https://sfo-private.spacesforce.com
#许可上传文件类型
FILE_TYPE=jpg,jpeg,png,gif,xlsx,doc,docx,xls,ppt,pptx,pdf,txt,bmp,rar,zip
# API项目主host 比如http://www.spacesforce.com 后面不带/
APP_URL=https://amc-dev.spacesforce.com
# 供应商标记 字符串如：adenew
OPERATOR_CODE=hgvyylst88
OPERATOR_GUID=A8D4F8F2-6810-39A1-09CF-D4307A2CE488
# 通信密钥
SECRET_TOKEN=uul0Ch6T3VkWdM4AtewakzczM2Y3ODQzM2FlMDA4N2E2MGNjNWIyYzg5MWQxMWY4MTQ4NWIxMGMyMzMxMDgyYjUxZDIzNTFiYjg2NDkxNTDnfg/v3rLQd9eEKgduT8R1UIb++t5SXsxdUVTAs3U3WIlHQV5WYhwJTiHu8an5chaDALAyMSSAbdk4P0VslYvJxpMVaP4oRcKM10W91Zg5GqGRBbMMI4lS1OXzXgu+e8k=
# laravel api访问域名后缀
URL_PREFIX=api
#微信公众号ID/密钥
APP_ID=wx9f173f83a13d46ca
APP_SECRET=78af7c7bdc38b5ecae766195625c2597
# 超管新yii2网关
SFO_GATEWAY=${SFO_GATEWAY}
#队列配置
MQ_HOST=47.110.90.211
MQ_PORT=11300
UV_TIME=30
# word 转 pdf 服务的host
WORD_PDF_HOST=http://120.27.146.165:8081/
# 当前代码库名称
REPO_NAME=sfoom_api
# version text
DEPLOY_PRODUCT_NAME=AMC ESSENTIAL
DEPLOY_PRODUCT_VERSION=v1.3.1.1
DEPLOY_FRONTEND_VERSION=v1.3.1.1
DEPLOY_BACKEND_VERSION=v1.3.1.1
#微信授权中心
WECHAT_AUTH_CENTER_HOST=https://dev-wechat.spacesforce.com
# 允许发送短信的环境
ENV_ALLOW_SEND_SMS= 
# 允许发送邮件的环境
ENV_ALLOW_SEND_EMAIL= 
```

### 2.2.2  amc-back-login
```
#项目名称
APP_NAME=amc-back-login
#项目ukey
APP_KEY=base64:Plf3RV08Llabslm2pZmsUuAV2v4tjDdMQqLGAYmVKHY=
#debug是否开启
APP_DEBUG=true
#应用环境
APP_ENV=dev
#日志渠道
LOG_CHANNEL=stack
#数据库连接配置
DB_CONNECTION=mysql
DB_HOST= 
DB_PORT= 
DB_DATABASE= 
DB_USERNAME= 
DB_PASSWORD= 
#缓存驱动方式
CACHE_DRIVER=redis
#session驱动方式
SESSION_DRIVER=file
#队列驱动方式
QUEUE_DRIVER=sync
#memcache配置
MEMCACHE_HOST=127.0.0.1
MEMCACHE_PORT=11211
#redis配置
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
#时区配置
DB_TIMEZONE=+08:00
APP_TIMEZONE=PRC
# API项目主host 比如http://www.spacesforce.com 后面不带/
APP_URL=https://amc-dev.spacesforce.com
# 运营商基础信息
OPERATOR_CODE=login
OPERATOR_GUID=EBF4C705-6F90-9C40-0DD4-79C7DE6D3CDF
# 通信密钥
SECRET_TOKEN=kEfeSSpn1R0ggA8UIkOUzGI3MTM0OGYxYWNmYTU5NzkxZWNjYjBkOGZjZDM1ZWJkNTk4NDI0YmM2N2ZkYTBlMDcxZDM0M2Q5ZmFiZWM5MzNepCsNKAhRyzLDuDtUcL5tlNuYvYanpn8pl7KAllAx5VQhhrreXQbJm4jXROeS3ZG3pVh+TKEMbpHmkkzU4l80P9R+IDwZrmC7jcvfdaqJOczO5vFVM+tn9BMnMDs6pUE=
#代码仓库地址
REPO_NAME=amc-login-back
#微信公众号ID/密钥
APP_ID=wx9f173f83a13d46ca
APP_SECRET=78af7c7bdc38b5ecae766195625c2597
#laravel api访问域名后缀
URL_PREFIX=api
#项目当前url
CURRENT_URL=https://amc-dev.spacesforce.com/api/
#业务超管访问url
SFO_SERVICE_URL=https://sam-dev.spacesforce.com/api
# 超管新yii2网关 下载证书
SFO_GATEWAY=${SFO_GATEWAY}
#七牛云配置
QN_ACCESS_KEY=KOwgBwHvXPRToWUTNPlUla28Nbe5k8OwVKEIb9lG
QN_SECRET_KEY=ELl_VfTSyqrcXQPAjkgMNq0iN39MZtQodmJmhDGn
QN_OUTER_CHAIN_PUBLIC=http://sfo-public.spacesforce.com
QN_OUTER_CHAIN_PRIVATE=http://sfo-private.spacesforce.com
#许可删除文件类型
FILE_TYPE=jpg,jpeg,png,gif,xlsx,doc,docx,xls,ppt,pptx,pdf,txt,bmp,rar,zip
# version text
DEPLOY_PRODUCT_NAME=AMC ESSENTIAL
DEPLOY_PRODUCT_VERSION=v1.3.1.1
DEPLOY_FRONTEND_VERSION=v1.3.1.1
DEPLOY_BACKEND_VERSION=v1.3.1.1
```

### 2.2.3 twt-back-core
```
#项目名称
APP_NAME=twt-back-core
#项目ukey
APP_KEY=base64:0mk1mvD5TxIaEm257TZlIFSaPXx410dTi3eZOO0HisQ=
#debug是否开启
APP_DEBUG=true
#日志渠道
LOG_CHANNEL=stack
#应用环境
APP_ENV=dev
#数据库连接配置 amc-dev (dev环境)
DB_CONNECTION=mysql
DB_HOST=47.108.52.57
DB_PORT=5712
DB_DATABASE=amc_dev
DB_USERNAME=amc_dev
DB_PASSWORD="4RRQsfS#eXfn#hda"
#缓存驱动方式
CACHE_DRIVER=redis
#session驱动方式
SESSION_DRIVER=file
#队列驱动方式
QUEUE_DRIVER=sync
#memcache配置
MEMCACHE_HOST=127.0.0.1
MEMCACHE_PORT=11211
#redis配置
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
#时区配置
DB_TIMEZONE=+08:00
APP_TIMEZONE=PRC
#项目当前url
CURRENT_URL=https://twt-dev.spacesforce.com/api/
#业务超管访问url
SFO_SERVICE_URL=https://sam-dev.spacesforce.com/api
#七牛云配置
QN_ACCESS_KEY=KOwgBwHvXPRToWUTNPlUla28Nbe5k8OwVKEIb9lG
QN_SECRET_KEY=ELl_VfTSyqrcXQPAjkgMNq0iN39MZtQodmJmhDGn
QN_OUTER_CHAIN_PUBLIC=https://sfo-public.spacesforce.com
QN_OUTER_CHAIN_PRIVATE=https://sfo-private.spacesforce.com
#文件许可上传类型
FILE_TYPE=jpg,jpeg,png,gif,xlsx,doc,docx,xls,ppt,pptx,pdf,txt,bmp,rar,zip
# API项目主host 比如http://www.spacesforce.com 后面不带/
APP_URL=https://amc-dev.spacesforce.com
# 供应商标记 字符串如：adenew
OPERATOR_CODE=hgvyylst88
OPERATOR_GUID=A8D4F8F2-6810-39A1-09CF-D4307A2CE488
# 通信密钥
SECRET_TOKEN=AuA1rWz7fsaDOofML9l46GVlZjdhM2M2MzI3ZDFkNzU1ZjAyZTIxYmRmMDZhMDZiZmJhYmNiMWExNGQzZmE5OGJlMTk2OWUzNTQ4NDI1N2TK7vAEpFTkihR2jC8s3vNQpzXrGD08p+u5tyUDuZzXFdpNTitLJGYQaiFzNIYMs5vx+6sijM/E1ACZ5J/6ftW1I8m2NJhYvlrBE5X4h38lwY8EDRvX4cRdoI+A2kgsgVk=
# laravel api访问域名后缀
URL_PREFIX=api
#微信公众号ID/密钥
APP_ID=wx9f173f83a13d46ca
APP_SECRET=78af7c7bdc38b5ecae766195625c2597
# 超管新yii2网关
SFO_GATEWAY=${SFO_GATEWAY}
#队列配置
MQ_HOST=47.110.90.211
MQ_PORT=11300
UV_TIME=30
# word 转 pdf 服务的host
#WORD_PDF_HOST=http://word2pdf-dev-service.common-service/
WORD_PDF_HOST=http://120.27.146.165:8081/
# 当前代码库名称
REPO_NAME=twt-back
# version text
DEPLOY_PRODUCT_NAME=AMC ESSENTIAL
DEPLOY_PRODUCT_VERSION=v1.3.1.1
DEPLOY_FRONTEND_VERSION=v1.3.1.1
DEPLOY_BACKEND_VERSION=v1.3.1.1
```

### 2.2.4 twt-back-login
```
#项目名称
APP_NAME=twt-back-login
#项目ukey
APP_KEY=base64:/QmOQ7MqSbVsV1QVdme5/6xF8tUuniTvgD5U0lqTBv8=
#debug是否开启
APP_DEBUG=true
#应用环境
APP_ENV=dev
#日志渠道
LOG_CHANNEL=stack
#数据库连接配置
DB_CONNECTION=mysql
DB_HOST=
DB_PORT= 
DB_DATABASE= 
DB_USERNAME=
DB_PASSWORD= 
#缓存驱动方式
CACHE_DRIVER=redis
#session驱动方式
SESSION_DRIVER=file
#队列驱动方式
QUEUE_DRIVER=sync
#memcache配置
MEMCACHE_HOST=127.0.0.1
MEMCACHE_PORT=11211
#redis配置
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
#时区配置
DB_TIMEZONE=+08:00
APP_TIMEZONE=PRC
#项目当前url
CURRENT_URL=https://twt-dev.spacesforce.com/api/
#业务超管访问url
SFO_SERVICE_URL=https://sam-dev.spacesforce.com/api
# 超管新yii2网关 下载证书
SFO_GATEWAY=${SFO_GATEWAY}
#七牛云配置
QN_ACCESS_KEY=KOwgBwHvXPRToWUTNPlUla28Nbe5k8OwVKEIb9lG
QN_SECRET_KEY=ELl_VfTSyqrcXQPAjkgMNq0iN39MZtQodmJmhDGn
QN_OUTER_CHAIN_PUBLIC=https://sfo-public.spacesforce.com
QN_OUTER_CHAIN_PRIVATE=https://sfo-private.spacesforce.com
#许可上传文件类型
FILE_TYPE=jpg,jpeg,png,gif,xlsx,doc,docx,xls,ppt,pptx,pdf,txt,bmp,rar,zip
# API项目主host 比如http://www.spacesforce.com 后面不带/
APP_URL=https://twt-dev.spacesforce.com
# 供应商标记 字符串如：adenew
OPERATOR_CODE=login
OPERATOR_GUID=EBF4C705-6F90-9C40-0DD4-79C7DE6D3CDF
# 当前代码库名称
REPO_NAME=twt-login-back
# 通信密钥
SECRET_TOKEN=3XQJtVOOlbSmdGKHKLt9BjM0NmNiZmNiZGFkNTBmYzNlYzAwOWI4ODM1YzljOGZmNWEwZTFmOTQxZmY0ZDI5MmRkZjE0ZjI3MzA3YjE5N2W+fM3Ol5RTMevcntra9h3tltsLqFdibEB/SyJMakjlUNug1bM2ZKe3vF3C97XJyTgvVRjRksoci+lmDS0Szhjh8Pz0uuyF2froeJXf2cPGUvdwYSl/d03LCUMCJ6aRXVA=
# laravel api访问域名后缀
URL_PREFIX=api
#微信公众号ID/密钥
APP_ID=wx9f173f83a13d46ca
APP_SECRET=78af7c7bdc38b5ecae766195625c2597
#队列配置
MQ_HOST=47.110.90.211
MQ_PORT=11300
UV_TIME=30
# word 转 pdf 服务的host
#WORD_PDF_HOST=http://word2pdf-dev-service.common-service/
WORD_PDF_HOST=http://120.27.146.165:8081/
# version text
DEPLOY_PRODUCT_NAME=AMC ESSENTIAL
DEPLOY_PRODUCT_VERSION=v1.3.1.1
DEPLOY_FRONTEND_VERSION=v1.3.1.1
DEPLOY_BACKEND_VERSION=v1.3.1.1
```

### 2.3 各项目依赖说明
* 各项目在通过git clone后，需要根据以下说明，加载项目所需要的依赖
#### 2.3.1 通用依赖
* 各项目根目录权限
```
chmod 755 -R storage
chmod 755 -R bootstrap
```
* 自动加载
```
composer dump-auto
```
* 初始化证书
```
php artisan init:cert
```

#### 2.3.2 amc-back-core
* 执行写入规则（ps：初始化系统数据依赖规则）
```
php artisan write:rules
```
* 初始化系统数据
```
php artisan command:init_amc
```
* 监测系统初始化
```
php artisan command:check_amc
```
*　将laravel定时任务添加到crontab
```
***** cd /path-to-your-project php artisan shedule:run >> /dev/null 2>&1
```
* 链接上传目录
```
php artisan storage:link
```

#### 2.3.3 amc-back-login
* 暂无
#### 2.3.4 
* 将laravel定时任务添加到crontab
```
***** cd /path-to-your-project php artisan schedule:run >> /dev/null 2>&amp;1
```
* 链接上传目录
```
php artisan storage:link
```
#### 2.3.5
* 暂无

## 三. AMC发布流程图

![avatar](https://gitlab.com/cgkei23/myblog/-/raw/master/source/amc-pipeline.jpg)


## 四. AMC分支流程图示
####前端分支
![avatar](https://gitlab.com/cgkei23/myblog/-/raw/master/source/git-front.png)
####后端分支
![avatar](https://gitlab.com/cgkei23/myblog/-/raw/master/source/git-back.png)



